## Data Collection

**/scripts**

- *cron_tweet.py*

Python script that ran every 15 minutes. In every execution it would request tweets from Twitter API, regarding one specific movie.
The 15 minutes rule exist because of Twitter API usage restrictions.

- *tweets.py*

Python script that picks every movie title from a pre-determined list and, for each one, gets its general information from the unofficial IMDB API, gets its reviews from Rotten Tomatoes (using a web crawler) and gets its box office information from Box Office Dojo (using a web crawler).

**/data**

- *info* contains a json file for each movie. Each file contains the general information about the movie (year of release, genre, country).
- *critics* contains a json file for each movie. Each file contains all its the reviews from Rotten Tomatoes.
- *box* contains a json file for each movie. Each file contains all weekly gross information from the movie.
- *tweets* contains a json file for each movie. Each file contains the result of the Twitter API request for that movie.

## Benchmarks

Contains the NoSQL and SQL Java Spring applications that loads the collected data. It also helped in the comparison of read operations between technologies.

## Databases

Contains .csv and .sql files, generated based on the Java Spring loading application.