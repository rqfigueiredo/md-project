package com.example.star_schema_nosql;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface PlatformRepository extends MongoRepository<PlatformDimension, Long> {
    PlatformDimension findByName(String name);
}
