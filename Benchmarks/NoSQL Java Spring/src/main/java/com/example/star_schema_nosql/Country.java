package com.example.star_schema_nosql;

import org.springframework.data.annotation.Id;

public class Country {

    @Id
    String id;

    String name;

    public Country(String name) {
        this.name = name;
    }

    public Country() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
