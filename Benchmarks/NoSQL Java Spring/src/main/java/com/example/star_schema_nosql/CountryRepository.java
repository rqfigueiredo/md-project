package com.example.star_schema_nosql;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;

public interface CountryRepository extends MongoRepository<Country, Long> {
    Country findByName(String name);
}
