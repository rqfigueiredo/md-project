package com.example.star_schema_nosql;

import org.springframework.data.annotation.Id;

public class WeeklyGross {

    @Id
    String id;

    String movieId;
    String countryId;
    int amount;
    int weekNumber;

    public WeeklyGross() {
    }

    public WeeklyGross(String movieId, String countryId, int amount, int weekNumber) {
        this.movieId = movieId;
        this.countryId = countryId;
        this.amount = amount;
        this.weekNumber = weekNumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMovieId() {
        return movieId;
    }

    public void setMovieId(String movieId) {
        this.movieId = movieId;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getWeekNumber() {
        return weekNumber;
    }

    public void setWeekNumber(int weekNumber) {
        this.weekNumber = weekNumber;
    }
}
