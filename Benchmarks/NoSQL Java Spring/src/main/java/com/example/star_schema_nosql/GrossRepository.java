package com.example.star_schema_nosql;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface GrossRepository extends MongoRepository<WeeklyGross, Long> {
    WeeklyGross findByMovieIdAndWeekNumberAndCountryId(String movieId, int weekNumber, String countryId);

    List<WeeklyGross> findByMovieId(String id);
}
