package com.example.star_schema_nosql;

import org.springframework.data.annotation.Id;

public class PlatformDimension {

    @Id
    String id;

    String name;
    String type;

    public PlatformDimension(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public PlatformDimension() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
