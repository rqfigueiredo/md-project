package com.example.star_schema_nosql;

import org.springframework.data.annotation.Id;

public class Publication {

    @Id
    String id;

    String movieId;
    String platformId;
    String dateId;

    String text;
    float polarity;
    float subjectivity;

    public Publication() {
    }

    public Publication(String movieId, String platformId, String dateId, String text, float polarity, float subjectivity) {
        this.movieId = movieId;
        this.platformId = platformId;
        this.dateId = dateId;
        this.text = text;
        this.polarity = polarity;
        this.subjectivity = subjectivity;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMovieId() {
        return movieId;
    }

    public void setMovieId(String movieId) {
        this.movieId = movieId;
    }

    public String getDateId() {
        return dateId;
    }

    public void setDateId(String dateId) {
        this.dateId = dateId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public float getPolarity() {
        return polarity;
    }

    public void setPolarity(float polarity) {
        this.polarity = polarity;
    }

    public float getSubjectivity() {
        return subjectivity;
    }

    public void setSubjectivity(float subjectivity) {
        this.subjectivity = subjectivity;
    }
}
