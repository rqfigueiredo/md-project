package com.example.star_schema_nosql;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface MovieRepository extends MongoRepository<MovieDimension, Long> {
    MovieDimension findByTitle(String title);

    List<MovieDimension> findByTitleLike(String title);
}
