package com.example.star_schema_nosql;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;

public interface AuthorRepository extends MongoRepository<AuthorDimension, Long> {
    AuthorDimension findByName(String author);
}
