package com.example.star_schema_nosql;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ReviewRepository extends MongoRepository<Review, Long> {
    List<Review> findByMovieId(String movieId);
}
