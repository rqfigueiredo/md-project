package com.example.star_schema_nosql;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PublicationRepository extends MongoRepository<Publication, Long> {
    List<Publication> findByMovieId(String id);
}
