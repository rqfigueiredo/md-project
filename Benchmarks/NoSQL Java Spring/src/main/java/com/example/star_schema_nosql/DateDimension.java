package com.example.star_schema_nosql;

import org.springframework.data.annotation.Id;

public class DateDimension {
    @Id
    private String id;

    private int day;
    private int month;
    private int year;

    public DateDimension() {}

    public DateDimension(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
