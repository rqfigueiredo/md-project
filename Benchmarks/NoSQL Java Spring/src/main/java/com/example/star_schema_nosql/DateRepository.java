package com.example.star_schema_nosql;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;

public interface DateRepository extends MongoRepository<DateDimension, Long> {
    DateDimension findByDayAndMonthAndYear(int day, int month, int year);
}
