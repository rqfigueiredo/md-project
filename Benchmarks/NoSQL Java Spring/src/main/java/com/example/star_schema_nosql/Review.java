package com.example.star_schema_nosql;

import org.springframework.data.annotation.Id;

public class Review {

    @Id
    String id;

    String platformId;
    String movieId;
    String authorId;

    String text;
    int isPositive;

    public Review() {
    }

    public Review(String platformId, String movieId, String authorId, String text, int isPositive) {
        this.platformId = platformId;
        this.movieId = movieId;
        this.authorId = authorId;
        this.text = text;
        this.isPositive = isPositive;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setMoviePlatformId(String platformId) {
        this.platformId = platformId;
    }

    public String getMovieId() {
        return movieId;
    }

    public void setMovieId(String movieId) {
        this.movieId = movieId;
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getIsPositive() {
        return isPositive;
    }

    public void setIsPositive(int isPositive) {
        this.isPositive = isPositive;
    }
}
