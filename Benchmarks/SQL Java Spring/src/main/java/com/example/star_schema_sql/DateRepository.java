package com.example.star_schema_sql;

import org.springframework.data.repository.CrudRepository;

public interface DateRepository extends CrudRepository<DateDimension, Long> {
    DateDimension findByDayAndMonthAndYear(int day, int month, int year);
}
