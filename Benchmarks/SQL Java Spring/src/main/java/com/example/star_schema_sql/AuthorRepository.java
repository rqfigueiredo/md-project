package com.example.star_schema_sql;

import org.springframework.data.repository.CrudRepository;

public interface AuthorRepository extends CrudRepository<AuthorDimension, Long> {
    AuthorDimension findByName(String author);
}
