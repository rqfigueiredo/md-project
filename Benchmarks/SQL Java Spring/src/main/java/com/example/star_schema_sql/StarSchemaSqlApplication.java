package com.example.star_schema_sql;

import com.example.star_schema_sql.queries.QueryA;
import com.example.star_schema_sql.queries.QueryB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SpringBootApplication
public class StarSchemaSqlApplication implements CommandLineRunner {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private static JSONParser parser;
    @Autowired
    private MovieRepository movies;
    @Autowired
    private GrossRepository grossRepository;
    @Autowired
    private PublicationRepository posts;
    @Autowired
    private ReviewRepository reviews;
    @Autowired
    private AuthorRepository authorRepository;
    @Autowired
    private DateRepository dateRepository;
    @Autowired
    private CountryRepository countryRepository;
    @Autowired
    private PlatformRepository platformRepository;

    public static void main(String[] args) {
        SpringApplication.run(StarSchemaSqlApplication.class, args);
    }

    private void getInfo() throws IOException, ParseException {
        parser = new JSONParser();

        File dir = new File(System.getProperty("user.dir") + "/data/info/");
        File[] directoryListing = dir.listFiles((directry, name) -> !name.equals(".DS_Store"));
        if (directoryListing != null) {
            for (File child : directoryListing) {
                Object obj = parser.parse(new FileReader(System.getProperty("user.dir") + "/data/info/" + child.getName()));

                JSONObject jsonObj = (JSONObject) obj;

                String title = (String) jsonObj.get("Title");
                String runtime = (String) jsonObj.get("Runtime");
                String language = (String) jsonObj.get("Language");

                int year = 0;
                try {
                    year = Integer.parseInt(((String) jsonObj.get("Released")).split(" ")[2]);
                } catch(Exception e) {
                    logger.info((String) jsonObj.get("Released"));
                }

                String actors = (String) jsonObj.get("Actors");
                String director = (String) jsonObj.get("Director");
                String genre = (String) jsonObj.get("Genre");

                MovieDimension movie = new MovieDimension(title, runtime, language, year, actors, director);
                movie.setGenre(genre);

                if(movie.getTitle() != null) {
                    if(movies.findByTitle(movie.getTitle()) == null)
                        movies.save(movie);
                }
            }
        }
    }

    private void getBoxOffice() throws IOException, ParseException {
        parser = new JSONParser();

        File dir = new File(System.getProperty("user.dir") + "/data/box");
        File[] directoryListing = dir.listFiles((directry, name) -> !name.equals(".DS_Store"));
        if (directoryListing != null) {
            for (File child : directoryListing) {
                if(child.length() == 0.0) continue;

                Object obj = parser.parse(new FileReader(System.getProperty("user.dir") + "/data/box/" + child.getName()));

                JSONArray array = (JSONArray) obj;
                JSONObject jsonObj = (JSONObject) array.get(0);

                JSONArray weeks = (JSONArray) jsonObj.get("weeks");
                JSONArray grosses = (JSONArray) jsonObj.get("grosses");

                String title = child.getName().split(".json")[0];

                int count = 0;
                for(Object week : weeks) {

                    int weekNumber = Integer.parseInt(((JSONArray) week).get(0).toString());

                    String grossString = (String)((JSONArray) grosses.get(count)).get(0);
                    String dollar = grossString.replace("$", "");
                    String grossFinal = dollar.replace(",", "");

                    int gross = Integer.parseInt(grossFinal);

                    MovieDimension m = movies.findByTitle(title);

                    if(m == null) {
                        if (child.getName().contains(":")) {
                            m = movies.searchByTitle(title.split(":")[1]);
                        } else {
                            m = movies.searchByTitle(title);
                        }
                    }

                    if(m != null) {
                        long movieId = m.getId();

                        Country country = countryRepository.findByName("USA");
                        if(country == null)
                            country = countryRepository.save(new Country("USA"));

                        if(grossRepository.findByMovieIdAndWeekNumberAndCountryId(movieId, weekNumber, country.getId()) == null)
                            grossRepository.save(new WeeklyGross(movieId, country.getId(), gross, weekNumber));
                    }

                    count ++;
                }
            }
        }
    }

    private void getReviews() throws IOException, ParseException {
        parser = new JSONParser();

        File dir = new File(System.getProperty("user.dir") + "/data/critics");
        File[] directoryListing = dir.listFiles((directry, name) -> !name.equals(".DS_Store"));
        if (directoryListing != null) {
            for (File child : directoryListing) {
                if (child.length() == 0.0) continue;

                Object obj = parser.parse(new FileReader(System.getProperty("user.dir") + "/data/critics/" + child.getName()));

                JSONArray array = (JSONArray) obj;
                array.remove(0);

                String title = child.getName().split(".json")[0];
                MovieDimension m = movies.findByTitle(title);
                if(m == null) {
                    if (child.getName().contains(":")) {
                        m = movies.searchByTitle(title.split(":")[1]);
                    } else {
                        m = movies.searchByTitle(title);
                    }
                }

                if(m == null) continue;

                for(Object reviewObj : array) {
                    JSONObject review = (JSONObject) reviewObj;
                    JSONArray authors = (JSONArray) review.get("author");

                    String text = review.get("text").toString();
                    if(text.length() > 250) {
                        text = text.substring(0, 250);
                    }
                    int isPositive = Integer.parseInt(review.get("is_fresh").toString());

                    if(authors.size() == 0) continue;

                    String authorName = (String) authors.get(0);

                    AuthorDimension author = authorRepository.findByName(authorName);
                    if(author == null)
                        author = authorRepository.save(new AuthorDimension(authorName));

                    PlatformDimension platform = platformRepository.findByName("Rotten Tomatoes");
                    if(platform == null)
                        platform = platformRepository.save(new PlatformDimension("Rotten Tomatoes", "Critic"));

                    reviews.save(new Review(platform.getId(), m.getId(), author.getId(), text, isPositive));
                }
            }
        }
    }

    private void getPublications() throws IOException, ParseException {
        parser = new JSONParser();

        PlatformDimension platform = platformRepository.findByName("Twitter");
        if(platform == null)
            platform = platformRepository.save(new PlatformDimension("Twitter", "Social"));

        File dir = new File(System.getProperty("user.dir") + "/data/tweets");
        File[] directoryListing = dir.listFiles((directry, name) -> !name.equals(".DS_Store"));
        if (directoryListing != null) {
            List<Publication> publications = new ArrayList<Publication>();
            for (File child : directoryListing) {
                if (child.length() == 0.0) continue;

                Object obj = parser.parse(new FileReader(System.getProperty("user.dir") + "/data/tweets/" + child.getName()));
                JSONArray array = (JSONArray) obj;

                String title = child.getName().split(".json")[0];
                MovieDimension m = movies.findByTitle(title);
                if(m == null) {
                    if (child.getName().contains(":")) {
                        m = movies.searchByTitle(title.split(":")[1]);
                    } else {
                        m = movies.searchByTitle(title);
                    }
                }

                if(m == null) continue;

                for(Object tweetObj : array) {

                    JSONObject tweet = (JSONObject) tweetObj;

                    String dateStrig = tweet.get("created_at").toString();

                    String text = tweet.get("text").toString();
                    float polarity = Float.parseFloat(tweet.get("polarity").toString());
                    float subjectivity = Float.parseFloat(tweet.get("subjectivity").toString());
                    int day = Integer.parseInt(dateStrig.split("-")[2].split(" ")[0]);
                    int month = Integer.parseInt(dateStrig.split("-")[1]);
                    int year = Integer.parseInt(dateStrig.split("-")[0]);

                    if(text.length() > 250) {
                        text = text.substring(0, 250);
                    }

                    DateDimension date = dateRepository.findByDayAndMonthAndYear(day, month, year);
                    if(date == null)
                        date = dateRepository.save(new DateDimension(day, month, year));

                    Publication p = new Publication(m.getId(), platform.getId(), date.getId(), text, polarity, subjectivity);
                    publications.add(p);
                }
                posts.saveAll(publications);
            }
        }
    }

    private void query_a() {
        Iterable<MovieDimension> movieList = movies.findAll();
        List<QueryA> queries = new ArrayList<QueryA>();

        for(MovieDimension movie: movieList) {

            QueryA query =  new QueryA();

            query.setTitle(movie.getTitle());

            List<Review> movieReviews = reviews.findByMovieId(movie.getId());

            int sum = 0;
            for(Review review: movieReviews) {
                sum += review.isPositive;
            }

            if(movieReviews.size() == 0) {
                query.setAvg_critic(0);
            }  else {
                query.setAvg_critic(sum * 100.0f / movieReviews.size());
            }

            List<Publication> moviePosts = posts.findByMovieId(movie.getId());

            int sum_pol = 0;
            int sum_obj = 0;
            for(Publication post: moviePosts) {
                sum_pol += post.getPolarity();
                sum_obj += post.getSubjectivity();
            }

            List<WeeklyGross> grossList = grossRepository.findByMovieId(movie.getId());

            sum = 0;
            for(WeeklyGross gross: grossList) {
                sum += gross.getAmount();
            }

            if(grossList.size() == 0) {
                query.setAvg_pol(0.0f);
                query.setAvg_sub(0.0f);
            } else {
                query.setAvg_pol(sum_pol * 1.0f / moviePosts.size());
                query.setAvg_sub(sum_obj * 1.0f / moviePosts.size());
            }

            query.setGross(sum);

            if(query.getAvg_pol() > 0.5f && query.getAvg_sub() < 0.3f && query.getAvg_critic() > 70.0f)
                queries.add(query);

            Collections.sort(queries);
        }
    }

    private void query_b() {

        /*
        select  year, count( if(is_positive = 1, 1, null) ) / count(is_positive) * 100 as avg_critic, avg(polarity) as avg_pol
        from    movie_dimension, review, publication
        where   year between 1950 and 2000 and movie_dimension.id = review.movie_id and movie_dimension.id = publication.movie_id
        and     char_length(review.text) > 50
        and     char_length(publication.text) > 15
        group by year;
        */

        Iterable<MovieDimension> movieList = movies.findAll();
        List<QueryB> queries = new ArrayList<QueryB>();

        for (MovieDimension movie : movieList) {

            QueryB query = new QueryB();

            query.setYear(movie.getYear());

            List<Review> movieReviews = reviews.findByMovieId(movie.getId());

            int sum = 0;
            for (Review review : movieReviews) {
                sum += review.isPositive;
            }

            if (movieReviews.size() == 0) {
                query.setAvg_critic(0);
            } else {
                query.setAvg_critic(sum * 100.0f / movieReviews.size());
            }

            List<Publication> moviePosts = posts.findByMovieId(movie.getId());

            int sum_pol = 0;
            for (Publication post : moviePosts) {
                sum_pol += post.getPolarity();
            }

            if (moviePosts.size() == 0) {
                query.setAvg_pol(0.0f);
            } else {
                query.setAvg_pol(sum_pol * 1.0f / moviePosts.size());
            }

            if (1955 < query.getYear() && query.getYear() < 2000)
                queries.add(query);

            Collections.sort(queries);
        }
    }

    private void query_b() {

        /*
        select  year, count( if(is_positive = 1, 1, null) ) / count(is_positive) * 100 as avg_critic, avg(polarity) as avg_pol
        from    movie_dimension, review, publication
        where   year between 1950 and 2000 and movie_dimension.id = review.movie_id and movie_dimension.id = publication.movie_id
        and     char_length(review.text) > 50
        and     char_length(publication.text) > 15
        group by year;
        */

        Iterable<MovieDimension> movieList = movies.findAll();
        List<QueryB> queries = new ArrayList<QueryB>();

        for (MovieDimension movie : movieList) {

            QueryB query = new QueryB();

            query.setYear(movie.getYear());

            List<Review> movieReviews = reviews.findByMovieId(movie.getId());

            int sum = 0;
            for (Review review : movieReviews) {
                sum += review.isPositive;
            }

            if (movieReviews.size() == 0) {
                query.setAvg_critic(0);
            } else {
                query.setAvg_critic(sum * 100.0f / movieReviews.size());
            }

            List<Publication> moviePosts = posts.findByMovieId(movie.getId());

            int sum_pol = 0;
            for (Publication post : moviePosts) {
                sum_pol += post.getPolarity();
            }

            if (moviePosts.size() == 0) {
                query.setAvg_pol(0.0f);
            } else {
                query.setAvg_pol(sum_pol * 1.0f / moviePosts.size());
            }

            if (1955 < query.getYear() && query.getYear() < 2000)
                queries.add(query);

            Collections.sort(queries);
        }
    }

    @Override
    public void run(String... args) throws Exception {

        /*
        long start = System.currentTimeMillis();
        this.getInfo();
        this.getBoxOffice();
        this.getReviews();
        this.getPublications();
        long end = System.currentTimeMillis();
        long elapsed = end - start;
        logger.info("Time elapsed: " + elapsed);
        */

        long start = System.currentTimeMillis();

        this.query_c();

        long end = System.currentTimeMillis();
        long elapsed = end - start;
        logger.info("Time elapsed: " + elapsed);

    }
}
