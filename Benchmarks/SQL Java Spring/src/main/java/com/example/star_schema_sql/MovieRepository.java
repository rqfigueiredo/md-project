package com.example.star_schema_sql;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;


public interface MovieRepository extends CrudRepository<MovieDimension, Long> {
    MovieDimension findByTitle(String title);

    @Query("SELECT m FROM MovieDimension m WHERE m.title LIKE CONCAT('%', :title, '%')")
    MovieDimension searchByTitle(@Param(value = "title") String title);
}
