package com.example.star_schema_sql;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class MovieDimension {
    @Id
    @GeneratedValue
    private long id;

    private String title;
    private String runtime;
    private String language;
    private int year;
    private String genre;
    private String actors;
    private String director;

    /*
    //Calculated properties

    private long profit;
    private int weeksInTheaters;
    private long socialCriticDiff;
    private long avgPolarity;
    private long avgSubjectivity;
    private long avgCriticScore;
    private int numberPosts;
    private int numberReviews;

    private String socialKeywords;
    private String reviewKeywords;
    */

    public MovieDimension() {}

    public MovieDimension(String title, String runtime, String language, int year, String actors, String director) {
        this.title = title;
        this.runtime = runtime;
        this.language = language;
        this.year = year;
        this.actors = actors;
        this.director = director;
    }

    public String getActors() {
        return actors;
    }

    public void setActors(String actors) {
        this.actors = actors;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRuntime() {
        return runtime;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}
