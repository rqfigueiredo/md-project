package com.example.star_schema_sql.queries;

import javax.management.Query;

public class QueryA implements Comparable<QueryA> {

    private String title;
    private float avg_pol;
    private float avg_sub;
    private float avg_critic;
    private int gross;

    public QueryA() {
    }

    public int getGross() {
        return gross;
    }

    public void setGross(int gross) {
        this.gross = gross;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getAvg_pol() {
        return avg_pol;
    }

    public void setAvg_pol(float avg_pol) {
        this.avg_pol = avg_pol;
    }

    public float getAvg_sub() {
        return avg_sub;
    }

    public void setAvg_sub(float avg_sub) {
        this.avg_sub = avg_sub;
    }

    public float getAvg_critic() {
        return avg_critic;
    }

    public void setAvg_critic(float avg_critic) {
        this.avg_critic = avg_critic;
    }

    public int compareTo(QueryA a)
    {
        return(this.getGross() - a.getGross());
    }

    public String toString() {
        return this.title + " | " + this.avg_pol + " | " + this.avg_sub + " | " + this.avg_critic + " | " + this.gross;
    }
}
