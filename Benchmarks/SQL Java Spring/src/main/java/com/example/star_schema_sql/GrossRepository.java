package com.example.star_schema_sql;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface GrossRepository extends CrudRepository<WeeklyGross, Long> {
    WeeklyGross findByMovieIdAndWeekNumberAndCountryId(long movieId, int weekNumber, long countryId);

    List<WeeklyGross> findByMovieId(long id);
}
