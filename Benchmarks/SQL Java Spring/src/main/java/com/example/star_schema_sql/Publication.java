package com.example.star_schema_sql;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Publication {

    @Id
    @GeneratedValue
    long id;

    long movieId;
    long platformId;
    long dateId;

    String text;
    float polarity;
    float subjectivity;

    public Publication() {
    }

    public Publication(long movieId, long platformId, long dateId, String text, float polarity, float subjectivity) {
        this.movieId = movieId;
        this.platformId = platformId;
        this.dateId = dateId;
        this.text = text;
        this.polarity = polarity;
        this.subjectivity = subjectivity;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getMovieId() {
        return movieId;
    }

    public void setMovieId(long movieId) {
        this.movieId = movieId;
    }

    public long getDateId() {
        return dateId;
    }

    public void setDateId(long dateId) {
        this.dateId = dateId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public float getPolarity() {
        return polarity;
    }

    public void setPolarity(float polarity) {
        this.polarity = polarity;
    }

    public float getSubjectivity() {
        return subjectivity;
    }

    public void setSubjectivity(float subjectivity) {
        this.subjectivity = subjectivity;
    }
}
