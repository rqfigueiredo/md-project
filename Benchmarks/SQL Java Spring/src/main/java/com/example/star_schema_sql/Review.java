package com.example.star_schema_sql;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Review {

    @Id
    @GeneratedValue
    long id;

    long platformId;
    long movieId;
    long authorId;

    String text;
    int isPositive;

    public Review() {
    }

    public Review(long platformId, long movieId, long authorId, String text, int isPositive) {
        this.platformId = platformId;
        this.movieId = movieId;
        this.authorId = authorId;
        this.text = text;
        this.isPositive = isPositive;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setMoviePlatformId(long platformId) {
        this.platformId = platformId;
    }

    public long getMovieId() {
        return movieId;
    }

    public void setMovieId(long movieId) {
        this.movieId = movieId;
    }

    public long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(long authorId) {
        this.authorId = authorId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getIsPositive() {
        return isPositive;
    }

    public void setIsPositive(int isPositive) {
        this.isPositive = isPositive;
    }
}
