package com.example.star_schema_sql;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class WeeklyGross {

    @Id
    @GeneratedValue
    long id;

    long movieId;
    long countryId;
    int amount;
    int weekNumber;

    public WeeklyGross() {
    }

    public WeeklyGross(long movieId, long countryId, int amount, int weekNumber) {
        this.movieId = movieId;
        this.countryId = countryId;
        this.amount = amount;
        this.weekNumber = weekNumber;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getMovieId() {
        return movieId;
    }

    public void setMovieId(long movieId) {
        this.movieId = movieId;
    }

    public long getCountryId() {
        return countryId;
    }

    public void setCountryId(long countryId) {
        this.countryId = countryId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getWeekNumber() {
        return weekNumber;
    }

    public void setWeekNumber(int weekNumber) {
        this.weekNumber = weekNumber;
    }
}
