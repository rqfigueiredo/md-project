package com.example.star_schema_sql;

import org.springframework.data.repository.CrudRepository;

public interface PlatformRepository extends CrudRepository<PlatformDimension, Long> {
    PlatformDimension findByName(String name);
}
