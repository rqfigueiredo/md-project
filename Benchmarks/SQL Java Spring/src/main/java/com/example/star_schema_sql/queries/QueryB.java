package com.example.star_schema_sql.queries;

public class QueryB implements Comparable<QueryB> {
    int year;
    float avg_critic;
    float avg_pol;

    public QueryB() {
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public float getAvg_critic() {
        return avg_critic;
    }

    public void setAvg_critic(float avg_critic) {
        this.avg_critic = avg_critic;
    }

    public float getAvg_pol() {
        return avg_pol;
    }

    public void setAvg_pol(float avg_pol) {
        this.avg_pol = avg_pol;
    }

    public int compareTo(QueryB a)
    {
        return(this.getYear() - a.getYear());
    }
}
