package com.example.star_schema_sql;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class PlatformDimension {

    @Id
    @GeneratedValue
    long id;

    String name;
    String type;

    public PlatformDimension(String name, String type) {
        this.type = type;
        this.name = name;
    }

    public PlatformDimension() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
