import scrapy
import sys

class CriticsSpider(scrapy.Spider):
    name = "critics"

    def __init__(self, *args, **kwargs):
        super(CriticsSpider, self).__init__(*args, **kwargs) 

        link = kwargs.get('link')
        self.start_urls = [link]

    def critic_parser(self, response):
        for critic in response.css('div.review_table_row'):
            
            is_fresh = critic.css('div.fresh').extract()

            if is_fresh != []:
                status = 1
            else:
                status = 0
            
            yield {
                'author': critic.css('a.articleLink').select('text()').extract(),
                'text': critic.css('div.the_review').select('text()')[0].extract()[1:],
                'is_fresh': status
            }
        
        next_page = response.css("a[class='btn btn-xs btn-primary-rt']::attr('href')").extract()[1]
        
        if next_page is not None:
            yield response.follow(next_page, self.critic_parser)

    def parse(self, response):

        print response.css('div.audience-info div::text')[3].extract().split("\n")[1].split("\n")[0].strip()

        yield {
            "title": self.title ,
            "tomatometer": response.css("span.meter-value.superPageFontColor span::text").extract_first(),

            "reviews_count": response.css('#scoreStats div span::text')[2].extract(),
            "fresh_count": response.css('#scoreStats div span::text')[4].extract(),
            "rotten_count": response.css('#scoreStats div span::text')[6].extract(),
            "avg_critic_rating": response.css('#scoreStats div::text')[1].extract()[response.css('#scoreStats div::text')[1].extract().index('/')-3:response.css('#scoreStats div::text')[1].extract().index('/')],

            "audience_score": response.css('div.meter-value span::text').extract_first().split('%')[0],
            "avg_audience_rating": response.css('div.audience-info div::text')[1].extract()[response.css('div.audience-info div::text')[1].extract().index('/')-3:response.css('div.audience-info div::text')[1].extract().index('/')],
            "user_ratings_count": response.css('div.audience-info div::text')[3].extract().split("\n")[1].split("\n")[0].strip()
        }

        next_page = response.css("#criticHeaders a::attr('href')").extract_first()
        if next_page is not None:
            yield response.follow(next_page, self.critic_parser)