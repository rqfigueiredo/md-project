import scrapy
import sys

class BoxSpider(scrapy.Spider):
    name = "box"

    def __init__(self, *args, **kwargs):
        super(BoxSpider, self).__init__(*args, **kwargs) 

        link = kwargs.get('link')
        self.start_urls = [link]

    def weekly_parser(self, response):

        year = response.css('#body center b font::text').extract()
        print response.css('#body center b font::text').extract()

        index = 0
        gross_index = 1
        week_index = 7

        grosses = []
        weeks = []

        for row in response.css('table.chart-wide tr td'):
            if row.css("a::attr('href')").extract_first() is not None:
                    continue

            if index == gross_index:
                grosses.append(row.css("font::text").extract())
                gross_index = gross_index + 8

            if index == week_index:
                weeks.append(row.css("font::text").extract())
                week_index = week_index + 8

            index = index + 1

        yield {
            "weeks": weeks,
            "grosses": grosses
        }

    def parse(self, response):

        for li in response.css('ul.nav_tabs li'):
            if li.css('li a::text').extract_first() == "Weekly":
                weekly = li.css("li a::attr('href')").extract_first()
                break

        if weekly is not None:
            yield response.follow(weekly, self.weekly_parser)
