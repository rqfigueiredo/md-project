import tweepy
import requests
import json
import scrapy
from google import google
import os
import subprocess
import os.path
from textblob import TextBlob

'''
auth = tweepy.OAuthHandler("752WbcFhy7rDevtlGMiKfJIx4", "mXuGfwVgitA0vnuuAPl6z3XelBQxy9O7OBDbqUiQtJKPHLR3Dp")
auth.set_access_token("773211197076275201-ldDy0z3UJPiCjrvkzSVfeiALdASbzqV", "bFz0tyTi6rJPPgMtHw504AR9K8NYtqKIqkVEoR8izJovc")

api = tweepy.API(auth)

for tweet in tweepy.Cursor(api.search, q = "Pirates Of Caribbean: On Stranger Tides -filter:retweets", rpp = 100).items(1000):
    blob = TextBlob(tweet.text)
    print "-----------------------------------"
    print "CREATED AT: "        + str(tweet.created_at)
    print "TEXT: "              + tweet.text
    print "SENTIMENT: "         + str(blob.sentiment)
    print "-----------------------------------"
'''
import sys
reload(sys)
sys.setdefaultencoding('utf8')


def get_popular(titles):
        get_popular_url = "https://api.themoviedb.org/3/movie/popular?api_key=868dedd44e170a7f0abf88bf569b3ab2"
        popular_response = requests.get(get_popular_url)
        
        for m in json.loads(popular_response.content)['results']:
                titles.append(m['title'])
        
        return titles

def get_top(titles):
        get_top_url = "https://api.themoviedb.org/3/movie/top_rated?api_key=868dedd44e170a7f0abf88bf569b3ab2"
        top_response = requests.get(get_top_url)
        
        for m in json.loads(top_response.content)['results']:
                titles.append(m['title'])
        
        return titles

def get_titles(titles):
        titles = get_popular(titles)
        titles = get_top(titles)

        return titles

def get_google_links(platform, title):
        print title
        if platform == "rotten":
                response = google.search("rotten tomatoes " + title)
                return response[0].link
        else:
                response = google.search("box office mojo " + title)
                return response[0].link

def get_titles():
        titles = []
        # If already have the movie titles, its unecessary to get them again
        if(os.stat('titles.json').st_size == 0):
                titles = get_titles(titles)
                with open('titles.json', 'w') as f:
                        for title in titles:
                                f.write("%s\n" % title)
        else:
                with open('titles.json') as f:
                        titles = f.read()[:-1].split("\n")
        
        return titles

def get_title_info(titles):
        for title in titles:
                if(not os.path.isfile("./info/" + title + ".json")):
                        info_response = requests.get("http://www.omdbapi.com/?apikey=c5d8ee41&t=" + title)
                        movie_info = json.loads(info_response.content)
                
                        # If already have the movie info, its unecessary to get them again
                        with open("./info/" + title + ".json", 'w') as f:
                                f.write(json.dumps(movie_info))

def main():

        titles = get_titles()
        get_title_info(titles)

        
        # Get RT reviews (web crawler)
        for m in titles:
                if(not os.path.isfile("./critics/" + m + ".json")):
                        subprocess.call(["scrapy", "runspider", "./crawlers/critics_spider.py", "-o", "./critics/" + m + ".json", "-a", "link=" + get_google_links("rotten", m), "-a", "title=" + m])
        
        
        # Get BO $ Gross (web crawler)
        for m in titles:
                if(not os.path.isfile("./box/" + m + ".json")):
                        subprocess.call(["scrapy", "runspider", "./crawlers/gross_spider.py", "-o", "./box/" + m + ".json", "-a", "link=" + get_google_links("box", m)])
        

    
main()