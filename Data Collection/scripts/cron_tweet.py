import tweepy
from textblob import TextBlob
import os.path
import datetime
import json
import sys
from random import randint

def to_datetime(s):
    date = s.split(" ")[0]
    time = s.split(" ")[1]

    d = {
        "year": date.split("-")[0], "month": date.split("-")[1], "day": date.split("-")[2],
        "hours": time.split(":")[0], "minutes": time.split(":")[1], "seconds": time.split(":")[2]
    }

    return datetime.datetime(int(d["year"]), int(d["month"]), int(d["day"]), int(d["hours"]), int(d["minutes"]), int(d["seconds"]))

auth = tweepy.OAuthHandler("752WbcFhy7rDevtlGMiKfJIx4", "mXuGfwVgitA0vnuuAPl6z3XelBQxy9O7OBDbqUiQtJKPHLR3Dp")
auth.set_access_token("773211197076275201-ldDy0z3UJPiCjrvkzSVfeiALdASbzqV", "bFz0tyTi6rJPPgMtHw504AR9K8NYtqKIqkVEoR8izJovc")
api = tweepy.API(auth)

with open(os.path.dirname(os.path.abspath(__file__)) + '/titles.json') as f:
    titles = f.read()[:-1].split("\n")

with open(os.path.dirname(os.path.abspath(__file__)) + '/has_tweets.json') as f:
    has_tweets = f.read()[:-1].split("\n")

# If it exists a movie that has no tweet data, then extract it
for title in titles:
    if not title in has_tweets:

        print title
        
        tweets = tweepy.Cursor(api.search, q = title + " movie -filter:retweets", lang='en', tweet_mode="extended", rpp = 10).items(1000)

        for tweet in tweets:
            blob = TextBlob(tweet.full_text)

            tweet_obj = {
                "created_at": str(tweet.created_at),
                "text": tweet.full_text,
                "polarity": str(blob.sentiment.polarity),
                "subjectivity": str(blob.sentiment.subjectivity)
            }

            list = []

            if os.path.isfile(os.path.dirname(os.path.abspath(__file__)) + '/tweets/' + title + '.json'):
                with open(os.path.dirname(os.path.abspath(__file__)) + '/tweets/' + title + '.json') as f:
                    list = json.loads(f.read())

            list.append(tweet_obj)

            with open(os.path.dirname(os.path.abspath(__file__)) + '/tweets/' + title + '.json', 'w') as f:
                f.write(json.dumps(list))

        has_tweets.append(title)
        print has_tweets
        with open(os.path.dirname(os.path.abspath(__file__)) + '/has_tweets.json', 'w') as f:
            for title in has_tweets:
                f.write("%s\n" % title)

        sys.exit()
